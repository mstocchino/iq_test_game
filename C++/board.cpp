#include <unistd.h>
#include <cstring>
#include "board.hpp"
//commento

board::board(int p_free_pos) {
  printf("%s p_free_pos=%d +\n", __FUNCTION__, p_free_pos);
  c_position_status[p_free_pos] = 0;
  c_solution= "SOLUZIONE -> "; 
}


int board::move_pin_back(int p_pin, int p_direction) {
  int l_next, l_nextnext=0;
  int *l_dir_vector = NULL;

  switch (p_direction) {
  case UP_RIGHT:
    l_dir_vector = c_next_UR;
    break;
  case RIGHT:
    l_dir_vector = c_next_R;
    break;
  case DOWN_RIGHT:
    l_dir_vector = c_next_DR;
    break;
  case DOWN_LEFT:
    l_dir_vector = c_next_DL;
    break;
  case LEFT:
    l_dir_vector = c_next_L;
    break;
  case UP_LEFT:
    l_dir_vector = c_next_UL;
    break;
  default:
    return 0;
  }
  l_next = l_dir_vector[p_pin];
  l_nextnext = l_dir_vector[l_next];
  c_position_status[p_pin] = 1;
  c_position_status[l_next] = 1;
  c_position_status[l_nextnext] = 0;
  c_pin_count++;
  c_count_move++;
  dump();   
  printf("restored %d\n", l_next);
  string str2 = c_solution.substr (0,c_solution.length()-6);
  c_solution=str2;
  //std::cout << c_solution;
  return 0;
}

int board::move_pin(int p_pin, int p_direction) {
  int l_next, l_nextnext=0;
  int *l_dir_vector = NULL;
  if(c_pin_count==1){
      return 0;
  }
  switch (p_direction) {
  case UP_RIGHT:
    //printf("%s + pin=%d p_direction=%s\n", __FUNCTION__, p_pin, "UP_RIGHT");
    l_dir_vector = c_next_UR;
    break;
  case RIGHT:
    //printf("%s + pin=%d p_direction=%s\n", __FUNCTION__, p_pin, "RIGHT");
    l_dir_vector = c_next_R;
    break;
  case DOWN_RIGHT:
    //printf("%s + pin=%d p_direction=%s\n", __FUNCTION__, p_pin, "DOWN_RIGHT");
    l_dir_vector = c_next_DR;
    break;
  case DOWN_LEFT:
    //printf("%s + pin=%d p_direction=%s\n", __FUNCTION__, p_pin, "DOWN_LEFT");
    l_dir_vector = c_next_DL;
    break;
  case LEFT:
    //printf("%s + pin=%d p_direction=%s\n", __FUNCTION__, p_pin, "LEFT");
    l_dir_vector = c_next_L;
    break;
  case UP_LEFT:
    //printf("%s + pin=%d p_direction=%s\n", __FUNCTION__, p_pin, "UP_LEFT");
    l_dir_vector = c_next_UL;
    break;
  default:
    return 0;
  }
  l_next = l_dir_vector[p_pin];       // il pin adiacente
  l_nextnext = l_dir_vector[l_next];  //il prossimo a quello adiacente

  if ((c_position_status[l_next] == 1) &&(c_position_status[l_nextnext] == 0)) { // se c'è il pin affianco e il prossimo è vuoto
    c_position_status[p_pin] = 0;           // free the current position
    c_position_status[l_next] = 0;            // remove next pin
    //printf("next=%d nextnext=%d\n", next, nextnext);
    c_pin_count--;
    c_position_status[l_nextnext] = 1; // lock the nextnext position
    c_count_move++;
    dump();
    printf("-----removed %d\n", l_next);
    char l_tok[7];
    sprintf(l_tok,"|%02d,%02d",p_pin,l_next);

    c_solution.append(string(l_tok));
    //std::cout << c_solution;
    if(c_pin_count==1){
        dump();
        printf("RISOLTO!!!\n");
        std::cout << c_solution;
        std::cout << std::endl; 
        c_count_solutions++;
        
        //exit(0);
    }
    return 1;
  } else {
    return 0;
  }
  return 0;
}

int board::self_play() {
  //printf("%s +\n", __FUNCTION__);
  for (int l_tmp_pin = 1; l_tmp_pin < 16; l_tmp_pin++) {
    if (c_position_status[l_tmp_pin] == 0)
      continue;
    for (int move = UP_RIGHT; move <= UP_LEFT; move++) {
      //usleep(5000);
      if (this->move_pin(l_tmp_pin, move) == 1) {
        if (self_play() == 0) {
          this->move_pin_back(l_tmp_pin, move);
        }
      }
    }
  }
  return 0;
}

void board::dump() {
  //system("clear");
  printf("________________________\n");
  printf("          %d\n", c_position_status[1]);
  printf("        %d  %d\n", c_position_status[2], c_position_status[3]);
  printf("     %d   %d   %d\n", c_position_status[4], c_position_status[5],c_position_status[6]);
  printf("   %d   %d   %d   %d\n", c_position_status[7], c_position_status[8],c_position_status[9], c_position_status[10]);
  printf(" %d   %d   %d   %d   %d\n", c_position_status[11], c_position_status[12],c_position_status[13], c_position_status[14], c_position_status[15]);
};

