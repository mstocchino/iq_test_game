#include <stdio.h>
#include <stdlib.h>

#include "board.hpp"


int main(){
    board* my_board=new board(1);
    my_board->dump();
    my_board->self_play();
    printf("HO PROVATO %d MOSSE\n",my_board->c_count_move);
    printf("HO TROVATO %d SOLUZONI\n",my_board->c_count_solutions);
    return 0;
}
