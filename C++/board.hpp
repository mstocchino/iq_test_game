#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

using std::string;
using std::ostringstream;

#define UP_RIGHT   1
#define RIGHT      2
#define DOWN_RIGHT 3
#define DOWN_LEFT  4
#define LEFT       5
#define UP_LEFT    6


class board{
    public:
        board(int p_free_pos);
        void dump();
        int move_pin(int p_pin,int p_direction);
        int move_pin_back(int p_pin,int p_direction);
        int self_play();
        int c_pin_count=14;
        int c_count_solutions=0;
        int c_count_move=0;
        string c_solution;

    
    private:
        //                       0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
        int c_position_status[16]={-1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 };
        int          c_next_L[16]={0 ,0 ,0 ,2 ,0 ,4 ,5 ,0 ,7 ,8 ,9 ,0 ,11,12,13,14};
        int          c_next_R[16]={0 ,0 ,3 ,0 ,5 ,6 ,0 ,8 ,9 ,10,0 ,12,13,14,15,0 };
        int         c_next_UR[16]={0 ,0 ,1 ,0 ,2 ,3 ,0 ,4 ,5 ,6 ,0 ,7 ,8 ,9 ,10,0 };
        int         c_next_UL[16]={0 ,0 ,0 ,1 ,0 ,2 ,3 ,0 ,4 ,5 ,6 ,0 ,7 ,8 ,9 ,10};        
        int         c_next_DR[16]={0 ,3 ,5 ,6 ,8 ,9 ,10,12,13,14,15,0 ,0 ,0 ,0 ,0 };
        int         c_next_DL[16]={0 ,2 ,4 ,5 ,7 ,8 ,9, 11,12,13,14,0 ,0 ,0 ,0 ,0 };

};

#if 0

                    1
                2       3
            4       5       6
        7       8       9       10 
    11      12      13      14      15


#endif